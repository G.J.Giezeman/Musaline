// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:
#include "musaline.hpp"
#include <iostream>
#include <vector>
#include <list>

struct MyElement {
    int value;
};

class MyCostCalculator1 {
    public:
        double match(MyElement const &a, MyElement const &b) const
        { return a.value==b.value? 1.2 : -1.0; }
        double gap_bft() const
        { return -2.0; }
        template <class Seq0, class Seq1>
        void preprocess(Seq0 const &, Seq1 const &) const
        {}
};

int main()
{
    try {
    musaline::LinearAligner<MyCostCalculator1> aligner;
    MyCostCalculator1 const &inner(aligner.get_cost_calculator());
    MyElement arr0[] = { 5,3,78,2,1,1,2};
    MyElement arr1[] = { 3,7,2,1,2};
    std::vector<MyElement> vec1(arr1,arr1+5);
    std::list<MyElement> lis1(vec1.begin(),vec1.end());
    double score=aligner.global_align(arr0, arr1).score;
    std::cout<<"Score a-a: "<<score<<"\n";
    score=aligner.global_align(arr0, arr1).score;
    std::cout<<"Score a-v: "<<score<<"\n";
    score=aligner.global_align(arr0, lis1).score;
    std::cout<<"Score a-l: "<<score<<"\n";
    } catch(Geert_cs_uu_nl::index_error const &ierr) {
        std::clog<<"Caught index error.\n";
        std::clog<<ierr.row_index()<<" "<<ierr.col_index()<<' '
            <<ierr.row_size()<<' '<<ierr.col_size()<<"\n";
        return 1;
    }
}


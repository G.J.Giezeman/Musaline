// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

#include "musaline.hpp"
#include <iostream>

class MyCostCalculator1 {
    public:
        void preprocess(int const *, int const *) {}
        double match(int a, int b) const
        {
            return a==b? 1.2 : -1.0;
        }
        double gap_bft() const
        { return -2.0; }
};

int main()
{
    musaline::LinearAligner<MyCostCalculator1> aligner;
    int seq0[] = { 5,3,78,2,1,1,2};
    int seq1[] = { 3,7,2,1,2};
    double score=aligner.global_align(seq0, seq1).score;
    std::cout<<"Score: "<<score<<"\n";
    MyCostCalculator1 const &inner(aligner.get_cost_calculator());
}


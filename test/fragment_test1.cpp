// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

// The usage function tests the requirements for a CostCalculator class
// that should be usable by FragmentAligner.

#include <iostream>
#include "note_calculator1.hpp"

using std::clog;

#include "boost/range.hpp"

    template <class CostCalculator, class Sequence0, class Sequence1>
    void usage(CostCalculator const &ccal1,
            Sequence0 const &seq_0, Sequence1 const &seq_1)
    {
        clog<<"Usage check.\n";
        // Copy constructor
        CostCalculator ccal(ccal1);
        // assignment operator
        ccal = ccal1;
        clog<<"Costcalculator copy construction OK.\n";
        ccal.preprocess(seq_0, seq_1);
        clog<<"Preprocessing OK.\n";
        size_t sz1 = boost::distance(seq_0);
        size_t sz2 = boost::distance(seq_1);
        clog<<"Sizes of sequences were retrieved as "<<sz1<<" and "<<sz2<<".\n";

        //auto s1cur = boost::const_begin(seq_0);
        typedef typename boost::range_iterator<const Sequence0>::type It0;
        typedef typename boost::range_iterator<const Sequence1>::type It1;
        It0 s1cur = boost::begin(seq_0);
        int i=2, j=6;


        vector<double> benefits =
            ccal.match_range_with1(seq_0[i], seq_1.begin(), seq_1.begin()+j);
        // return 0 or more doubles: benefit for matching note_0 with
        // last 2+k seq_1 elements, where k=index in resulting vector.

        for (size_t i=0; i!=sz1; ++i) {
            //auto s2cur = boost::const_begin(seq_1);
            It1 s2cur = boost::const_begin(seq_1);
            for (size_t j=0; j!=sz2; ++j) {
                double benefit = ccal.match(*s1cur, *s2cur);
                ++s2cur;
            }
            ++s1cur;
        }
        clog<<"All is well.\n";
    }

int main()
{
    vector<Note> seq0(10), seq1(15);
    usage(NoteCostCalculator(), seq0, seq1);
}

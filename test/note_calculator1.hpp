#ifndef NOTE_CALCULATOR0_11720_24941
#define NOTE_CALCULATOR0_11720_24941

// Author: Geert-Jan Giezeman <G.J.Giezeman@uu.nl>
// Copyright: Utrecht University, 2015


#include <vector>
#include <algorithm>
using std::vector;

struct Note
{
    int pitch;
    int duration;
};

struct NoteCostCalculator {
    void preprocess(vector<Note> const &seq1, vector<Note> const &seq2) {}
    template <class Seq1, class Seq2>
    void preprocess(Seq1 const &seq1, Seq2 const &seq2) {}

    double match_gap_with1(Note const &n) 
    {
        return -n.duration;
    }

    double match_gap_with2(Note const &n) 
    {
        return -n.duration;
    }

    template <class Iter>
    std::vector<double> match_range_with1(Note const &n, 
            Iter begin,
            Iter cur)
    {
        std::vector<double> result;
        int total_duration = 0;
        int matching_duration = 0;
        int i=0;
        while (cur != begin) {
            --cur;
            ++i;
            if (cur->pitch ==n.pitch) {
                matching_duration += cur->duration;
            }
            total_duration += cur->duration;
            if (i>1) {
                int max_duration=std::max (total_duration,n.duration);
                result.push_back(2.0*matching_duration - max_duration);
            }
            if (total_duration>=n.duration)
                break;
        }
        return result;
    }

    template <class Iter>
    std::vector<double> match_range_with2(
            Iter begin,
            Iter cur, Note const &n)
    {
        return match_range_with1(n,begin,cur);
    }

    double match(Note const &n1, Note const &n2)
    {
        auto minmaxpair = std::minmax (n1.duration, n2.duration);
        if (n1.pitch == n2.pitch) {

            return 2.0*minmaxpair.first - minmaxpair.second;
        } else {
            return -1.0*minmaxpair.second;
        }
    }
};

// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:
#endif // NOTE_CALCULATOR0_11720_24941

// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

#include "musaline.hpp"
#include "note_calculator1.hpp"
#include <vector>
#include <iostream>
#include <string>

using std::string;
using std::cout;

namespace m = musaline;

std::ostream & operator<<(std::ostream &os, Note const & nt)
{
    return os<<"<"<<nt.pitch<<" "<<nt.duration<<">";
}

/*
 * In this file we explain how to use the alignment library.
 * An aligner takes two sequences of symbols.
 * The outputs states which symbols align with a symbol of the other sequence
 * and which align with a gap, i.e., do not align with another symbol.
 * In this example we take a character as symbol and a std::string as sequence.
 * The requirement for a sequence is that it is a boost:range of symbols.
 * See www.boost.org for the range library.
 */

void write_string_alignment(char const *tp, m::Alignment const &al, 
         Note const *seq1, Note const *seq2)
{

    // Print the strings and the resulting alignment score (a double).
    cout<<tp<<" score: "<<al.score<<"\n";
    // We use the al.matches member to visualise the alignment.
    // This member is a vector of Match:
    // n[0] and n[1]: the number of elements of seq1 and seq2 involved in the match.
    // score: the contibution to the score for this match.
    // The symbols are characters (in this case, where the sequence is a string).
    int i1=al.start[0], i2=al.start[1];
    for (size_t i=0; i!=al.matches.size(); ++i) {
        m::Match const &match = al.matches[i];
        cout<<"  "<<match.score<<"\t| ";
        if (match.n[0]==0) {
            cout<<"-";
        } else {
            cout<<seq1[i1++];
            for (int i=1; i<match.n[0]; ++i) {
                cout<<","<<seq1[i1++];
            }
        }
        cout<<"\t| ";
        if (match.n[1]==0) {
            cout<<"-";
        } else {
            cout<<seq2[i2++];
            for (int i=1; i<match.n[1]; ++i) {
                cout<<","<<seq2[i2++];
            }
        }
        cout<<"\n";
    }
}

/*
 * do_one_alignment uses an arbitrary aligner to align two strings
 * and outputs the result.
 * Template parameter Aligner must be an aligner that takes strings as input.
 */
template <class Aligner, class Seq1, class Seq2>
void do_one_alignment(
        Aligner const & aligner,
        Seq1 const &seq1, Seq2 const &seq2)
{
    // Run the aligner with the two strings.
    // The result is an Alignment struct, which has members 'score' and 'a'.
    write_string_alignment("Global alignment",
            aligner.global_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one end off alignment",
            aligner.cut_one_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one begin off alignment",
            aligner.cut_one_begin_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq2, seq1), seq2, seq1); 
    write_string_alignment("Initial alignment",
            aligner.initial_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("End alignment",
            aligner.end_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Semi alignment",
            aligner.semi_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Partial alignment",
            aligner.partial_align(seq1, seq2), seq1, seq2); 
    /*
    */
}

/*
 * The StringCost class is used as a parameter for the Alignment.
 */

Note notes1[] = { {1,24}, {2,6},{3,6},{2,6},{4,6},{5,24}};
Note notes2[] = { {2,24},{5,48}};

void test0()
{
    // The GlobalLinAligner takes two parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 is the benefit for opening a gap
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** ConsolidatingAligner ***\n";
    m::ConsolidatingAligner<NoteCostCalculator> aligner;
    // Now align a few sequences.
    do_one_alignment(aligner,notes1, notes2);
}

/*
void test1()
{
    // The AffineAligner takes three parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 and -0.5 are the benefit for opening a gap
    // and for extending a gap.
    // Behind these two parameters a third one can be supplied: an instance
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** AffineAligner ***\n";
    m::AffineAligner<StringCost> aligner(-2.0, -0.5);
    // Now align a few sequences.
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}
*/


int main()
{
    test0();
}

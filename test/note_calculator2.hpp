#ifndef NOTE_CALCULATOR1_24308_26526
#define NOTE_CALCULATOR1_24308_26526

// Author: Geert-Jan Giezeman <geert@cs.uu.nl>
// Copyright: Utrecht University, 2012


#include <vector>
using std::vector;

struct Note
{
    int pitch;
    int duration;
};

struct NoteCostCalculator {
    void preprocess(vector<Note> const &seq1, vector<Note> const &seq2) {}
    template <class Seq1, class Seq2>
    void preprocess(Seq1 const &seq1, Seq2 const &seq2) {}

    double match_gap_with1(Note const &n) 
    {
        return -n.duration;
    }

    double match_gap_with2(Note const &n) 
    {
        return -n.duration;
    }

    template <class Iter>
    std::vector<double> match_range_with1(Note const &n, 
            Iter begin,
            Iter cur)
    {
        std::vector<double> result;
        int total_duration = 0;
        int matching_duration = 0;
        int mis_duration = 0;
        int i=0;
        while (cur != begin) {
            --cur;
            ++i;
            if (cur->pitch ==n.pitch) {
                matching_duration += cur->duration;
            } else {
                mis_duration += cur->duration;
            }
            total_duration += cur->duration;
            if (i>1) {
                if (total_duration>n.duration) {
                    int excess_duration = matching_duration-n.duration;
                    if (excess_duration > 0) {
                        mis_duration += excess_duration;
                        matching_duration = n.duration;
                    }
                    result.push_back(3.0*matching_duration
                            - 1.0*(total_duration-matching_duration));
                } else {
                    result.push_back(3.0*matching_duration
                            - 1.0*(n.duration-matching_duration));
                }
            }
            if (total_duration>=n.duration)
                break;
        }
        return result;
    }

    template <class Iter>
    std::vector<double> match_range_with2(
            Iter begin,
            Iter cur, Note const &n)
    {
        return match_range_with1(n,begin,cur);
    }

    double match(Note const &n1, Note const &n2)
    {
        int overlap;
        int overhang = n1.duration-n2.duration;
        if (overhang >=0) {
            overlap = n2.duration;
        } else {
            overhang = -overhang;
            overlap = n1.duration;
        }
        if (n1.pitch == n2.pitch) {
            return 3.0*overlap - 1.0*overhang;
        } else {
            return -1.0*(overlap + overhang);
        }
    }
};

// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:
#endif // NOTE_CALCULATOR1_24308_26526

// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

// This program computes the same as string_example, but it presents the
// results as an html file. For comments, see the string_example file.

#include "musaline.hpp"
#include <vector>
#include <iostream>
#include <string>

using std::string;
using std::cout;

namespace m = musaline;

// Write one aligned sequence as the of a table.
// Mark cells depending on their content:
// void: not a part of the sequence
// skip: a part of the sequence that is not matched
// normal: the matched part of the sequence
void write_string_row(m::Alignment const &al, 
        string const &seq, int idx, size_t other_size)
{

    const int oidx=1-idx;
    size_t outro1=seq.size()-al.end[idx];
    size_t outro2=other_size-al.end[oidx];

    cout<<"<tr>\n";
    for (size_t s=al.start[idx]; s<al.start[oidx]; ++s){
        cout<<"<td class=\"void\"> </td>";
    }
    size_t i1=0;
    for ( ;i1<al.start[idx]; ++i1) {
        cout<<"<td class=\"skip\">"<<seq[i1]<<"</td>\n";
    }
    for (size_t i=0; i!=al.matches.size(); ++i) {
        if (al.matches[i].n[idx]==0){
            cout<<"<td> </td>\n";
        } else {
            cout<<"<td>"<<seq[i1++]<<"</td>\n";
        }
    }
    assert(i1==al.end[idx]);
    for ( ;i1<seq.size(); ++i1) {
        cout<<"<td class=\"skip\">"<<seq[i1]<<"</td>\n";
    }
    for (size_t s=outro1; s<outro2; ++s){
        cout<<"<td class=\"void\"> </td>";
    }
    cout<<"</tr>\n";
}

void write_string_alignment(char const *tp, m::Alignment const &al, 
        string const &seq1, string const &seq2)
{
    cout<<"<h3>"<<tp<<"</h3>\n<table class=\"strmatch\">\n";
    write_string_row(al, seq1, 0, seq2.size());
    write_string_row(al, seq2, 1, seq1.size());
    cout<<"</table>\n<p>Score: "<<al.score<<"</p>\n";
    cout<<"<p>"<<al.start[0]<<" "<<al.end[0]<<" "<<seq1<<"</p>\n";
    cout<<"<p>"<<al.start[1]<<" "<<al.end[1]<<" "<<seq2<<"</p>\n";


}

template <class Aligner>
void do_one_alignment(
        Aligner const & aligner,
        string const &seq1, string const &seq2)
{
    write_string_alignment("Global alignment",
            aligner.global_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one end off alignment",
            aligner.cut_one_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one begin off alignment",
            aligner.cut_one_begin_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq2, seq1), seq2, seq1); 
    write_string_alignment("Semi alignment",
            aligner.semi_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Initial alignment",
            aligner.initial_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("End alignment",
            aligner.end_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Partial alignment",
            aligner.partial_align(seq1, seq2), seq1, seq2); 
}

class StringCost {
    public:
        void preprocess(string const&, string const&) {}
        double match(char a, char b) const
        {
            return a==b? 1.0 : -1.0;
        }
        double gap_bft() const
        { return -2.0;}
        double ext_bft() const
        { return -0.5;}
};

void test0()
{
    cout<<"<h2>LinearAligner</h2>\n";
    m::LinearAligner<StringCost> aligner;
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}

void test1()
{
    cout<<"<h2>AffineAligner</h2>\n";
    m::AffineAligner<StringCost> aligner;
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}


int main()
{
    cout<<"<html>\n<head>\n"
        " <title>Text matching results</title>\n"
    " <style type=\"text/css\">\n body {background-color:white;}\n"
        " .strmatch {background-color:yellow;}\n"
        ".skip {background-color:white;}\n"
        ".void {background-color:gray;}\n"
        "</style>\n"
        "</head>\n<body>\n<h1>Text matching</h1>";
    test0();
    test1();
    cout<<"</body>\n</html>\n";
}

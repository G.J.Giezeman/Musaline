// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

#include "musaline_fragment.hpp"
#include "note_calculator1.hpp"
#include <vector>
#include <iostream>
#include <string>

//using std::string;
using std::cout;
typedef std::vector<Note> NSeq;

namespace m = musaline;

/*
 * In this file we explain how to use the alignment library.
 * An aligner takes two sequences of symbols.
 * The outputs states which symbols align with a symbol of the other sequence
 * and which align with a gap, i.e., do not align with another symbol.
 * In this example we take a character as symbol and a NSeq as sequence.
 * The requirement for a sequence is that it is a boost:range of symbols.
 * See www.boost.org for the range library.
 */

void write_string_alignment(char const *tp, m::Alignment2 const &al, 
        NSeq const &seq1, NSeq const &seq2)
{
    // Print the strings and the resulting alignment score (a double).
//    cout<<tp<<" score of \""<<seq1<<"\" and \""
//        <<seq2<<"\": "<<al.score<<"\n";
    // We use the al.a member to visualise the alignment.
    // This member is a vector of AlignType, which can have three values:
    // BothSeqs: align next of seq1 with next of seq2.
    // FirstSeqWithGap: align next symbol of seq1 with a gap.
    // SecondSeqWithGap: align next symbol of seq2 with a gap.
    // The symbols are characters (in this case, where the sequence is a string).
    int i1=al.start[0], i2=al.start[1];
    for (size_t i=0; i!=al.a.size(); ++i) {
        switch (al.a[i].ak) {
        case m::BothSeqs:
            //cout<<seq1[i1++]<<' '<<seq2[i2++]<<"\n";
            break;
        case m::FirstSeqWithGap:
            //cout<<seq1[i1++]<<" -\n";
            break;
        case m::SecondSeqWithGap:
            //cout<<"- "<<seq2[i2++]<<"\n";
            break;
        }
    }
}

/*
 * do_one_alignment uses an arbitrary aligner to align two strings
 * and outputs the result.
 * Template parameter Aligner must be an aligner that takes strings as input.
 */
template <class Aligner>
void do_one_alignment(
        Aligner const & aligner,
        NSeq const &seq1, NSeq const &seq2)
{
    // Run the aligner with the two strings.
    // The result is an Alignment struct, which has members 'score' and 'a'.
    write_string_alignment("Global alignment",
            aligner.global_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one end off alignment",
            aligner.cut_one_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one begin off alignment",
            aligner.cut_one_begin_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq2, seq1), seq2, seq1); 
    write_string_alignment("Initial alignment",
            aligner.initial_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("End alignment",
            aligner.end_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Semi alignment",
            aligner.semi_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Partial alignment",
            aligner.partial_align(seq1, seq2), seq1, seq2); 
}


void test0()
{
    // The GlobalLinAligner takes two parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 is the benefit for opening a gap
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** FragmentAligner ***\n";
    m::FragmentAligner<NoteCostCalculator> aligner(-2.0);
    // Now align a few sequences.
    NSeq seq0, seq1;
    do_one_alignment(aligner,seq0,seq1);
    NoteCostCalculator const &inner(aligner.get_cost_calculator());
}


int main()
{
    test0();
}

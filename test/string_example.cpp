// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

#include "musaline.hpp"
#include <vector>
#include <iostream>
#include <string>

using std::string;
using std::cout;

namespace m = musaline;

/*
 * In this file we explain how to use the alignment library.
 * An aligner takes two sequences of symbols.
 * The outputs states which symbols align with a symbol of the other sequence
 * and which align with a gap, i.e., do not align with another symbol.
 * In this example we take a character as symbol and a std::string as sequence.
 * The requirement for a sequence is that it is a boost:range of symbols.
 * See www.boost.org for the range library.
 */

void write_string_alignment(char const *tp, m::Alignment const &al, 
        string const &seq1, string const &seq2)
{
    // Print the strings and the resulting alignment score (a double).
    cout<<tp<<" score of \""<<seq1<<"\" and \""
        <<seq2<<"\": "<<al.score<<"\n";
    // We use the al.matches member to visualise the alignment.
    // This member is a vector of Match:
    // n[0] and n[1]: the number of elements of seq1 and seq2 involved in the match.
    // score: the contibution to the score for this match.
    // The symbols are characters (in this case, where the sequence is a string).
    int i1=al.start[0], i2=al.start[1];
    for (size_t i=0; i!=al.matches.size(); ++i) {
        m::Match const &match = al.matches[i];
        cout<<match.score<<"\t";
        if (match.n[0]==0) {
            cout<<"-";
        } else {
            cout<<seq1[i1++];
        }
        cout<<' ';
        if (match.n[1]==0) {
            cout<<"-";
        } else {
            cout<<seq2[i2++];
        }
        cout<<"\n";
    }
}

/*
 * do_one_alignment uses an arbitrary aligner to align two strings
 * and outputs the result.
 * Template parameter Aligner must be an aligner that takes strings as input.
 */
template <class Aligner>
void do_one_alignment(
        Aligner const & aligner,
        string const &seq1, string const &seq2)
{
    // Run the aligner with the two strings.
    // The result is an Alignment struct, which has members 'score' and 'a'.
    write_string_alignment("Global alignment",
            aligner.global_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one end off alignment",
            aligner.cut_one_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut first end off alignment",
            aligner.cut_first_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut second end off alignment",
            aligner.cut_second_end_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Cut one begin off alignment",
            aligner.cut_one_begin_off_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Inside alignment",
            aligner.inside_align(seq2, seq1), seq2, seq1); 
    write_string_alignment("Initial alignment",
            aligner.initial_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("End alignment",
            aligner.end_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Semi alignment",
            aligner.semi_align(seq1, seq2), seq1, seq2); 
    write_string_alignment("Partial alignment",
            aligner.partial_align(seq1, seq2), seq1, seq2); 
}

/*
 * The StringCost class is used as a parameter for the Alignment.
 */
class StringCost {
    public:
        // The default constructor and copy constructor are generated automatically.
        // They are used by the libary.

        // preprocess takes two sequences. In this example it does nothing.
        // The function must be supplied.
        void preprocess(string const&, string const&) {}
        // 'match' computes the benefit of matching symbols a and b.
        // A higher value is a better match.
        double match(char a, char b) const
        {
            return a==b? 1.0 : -1.0;
        }
        double gap_bft() const
        { return -2.0;}
        double match_gap_with1(char ) const
        { return -2.0;}
        double match_gap_with2(char ) const
        { return -2.0;}
        double ext_bft() const
        { return -0.5;}
};

void test0()
{
    // The GlobalLinAligner takes two parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 is the benefit for opening a gap
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** LinearAligner ***\n";
    m::LinearAligner<StringCost> aligner;
    // Now align a few sequences.
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}

void test1()
{
    // The AffineAligner takes three parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 and -0.5 are the benefit for opening a gap
    // and for extending a gap.
    // Behind these two parameters a third one can be supplied: an instance
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** AffineAligner ***\n";
    m::AffineAligner<StringCost> aligner;
    // Now align a few sequences.
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}

void test2()
{
    // The GlobalLinAligner takes two parameters:
    // A template parameter for the cost function class.
    // The parameter -2.0 is the benefit for opening a gap
    // of the cost class. This defaults to a default constructed instance.
    cout<<"*** LinearAligner2 ***\n";
    m::LinearAligner2<StringCost> aligner;
    // Now align a few sequences.
    do_one_alignment(aligner,"AACG","ACGTA");
    do_one_alignment(aligner,"CAGCACTTGGATTCTCGG","CAGCGTGG");
    do_one_alignment(aligner,"als de lente komt","alle tekst");
}


int main()
{
    test0();
    test1();
    test2();
}

// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

// Author: Geert-Jan Giezeman <geert@cs.uu.nl>
// Copyright: Utrecht University, 2012

#include "csuu_matrix.hpp"
#include <iostream>

using namespace Geert_cs_uu_nl;
using std::cout;

template <class Mat>
void mtrtest(Mat &mat)
{
    mat(0,1)=1;
    mat(1,2)=2;
    mat(2,3)=3;
    typename Mat::CursorType crs1 = mat.get_cursor(0,1);
    typename Mat::CursorType crs(crs1);

    cout<<crs.row()<<" "<<crs.col()<<" "<<mat(crs)<<"\n";
    crs.inc_col();
    cout<<crs.row()<<" "<<crs.col()<<" "<<mat(crs)<<"\n";
    mat(crs)=15;
    cout<<crs.row()<<" "<<crs.col()<<" "<<mat(crs)<<"\n";
    crs.inc_row();
    cout<<crs.row()<<" "<<crs.col()<<" "<<mat(crs)<<"\n";
}

void triangtest()
{
    //typedef Triangular_URM_matrix<int> Mat;
    //Mat tmat(4,0);
    Triangular_URM_matrix<int> tmat(4,0);
    mtrtest(tmat);
    /*
    tmat(0,1)=1;
    tmat(1,2)=2;
    tmat(2,3)=3;
    Mat::CursorType crs = tmat.get_cursor();
    cout<<crs.row()<<" "<<crs.col()<<" "<<tmat(crs)<<"\n";
    crs.inc_col();
    cout<<crs.row()<<" "<<crs.col()<<" "<<tmat(crs)<<"\n";
    tmat(crs)=15;
    cout<<crs.row()<<" "<<crs.col()<<" "<<tmat(crs)<<"\n";
    crs.inc_row();
    cout<<crs.row()<<" "<<crs.col()<<" "<<tmat(crs)<<"\n";
    */
}

void gentest()
{
    Matrix<double> mat1(5,4,Uninit());
    mtrtest(mat1);
    Matrix<double>::CursorType crs = mat1.get_cursor();
    cout<<mat1(0,0)<<"\n";
    cout<<mat1(crs)<<"\n";
    crs.inc_col();
    cout<<mat1(crs)<<"\n";
    crs.inc_row();
    cout<<mat1(crs)<<"\n";
}

int main()
{
    gentest();
    triangtest();
    Square_matrix<int> sqm1(2,0);
    sqm1(0,1)=3;
    Square_matrix<int> sqm2(sqm1);
    Square_matrix<int> sqm3;
    sqm3=sqm2;
    if (sqm3(0,1)!=3)
        return 1;
    return 0;
}

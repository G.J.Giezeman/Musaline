// vim:set shiftwidth=4 softtabstop=4 expandtab cindent:

// Author: Geert-Jan Giezeman <geert@cs.uu.nl>
// Copyright: Utrecht University, 2012


In order to compare two sequences, we can align the elements of the
sequences to each other. Given a cost function for aligning elements, this
gives a similarity measure between sequences. The similarity is computed
with the Needleman-Wunsch algorithm and variants.

This library can align arbitrary sequences of arbitrary elements. The user
must supply a class for computing the cost of matching elements.
There are several cost functions supplied for the music domain, e.g. for
aligning melodies (sequences of notes).

This file explains the interface. Perhaps it is a good idea to first study
the example program test/string_example.cpp. This program is well commented
and explains most of the library. The output of the program (or actually,
of a similar program that outputs html), can be found in
test/text_result.html.

namespace musaline {

    enum AlignType {FirstSeqWithGap=0, SecondSeqWithGap=1, BothSeqs};
    struct Alignment {
        double score;
        size_t start[2], end[2];
        std::vector<AlignType> a;
    };


/*
    An Alignment records how two sequences (seq_0 and seq_1) are aligned.
    score: the computed similarity value for the alignment.
    start[i] and end[i] indicate the subsequence that was matched of seq_i.
    start indicates the first element and end points one after the last
    element. start[i]<=end[i], and end[i]-start[i] is the length of the
    subsequence that is matched.

    The vector 'a' holds the alignment types: an element of a sequence can
    align with an element of the other sequence (BothSeqs) or with a
    gap (FirstSeqWithGap and SecondSeqWithGap). The values 0 and 1 for FirstSeqWithGap
    and SecondSeqWithGap can be relied upon.
*/


    template <class CostCalculator>
    class LinearAligner {
    public:
        LinearAligner(double gap_benefit, 
                CostCalculator ccal = CostCalculator())
        : m_gap_bft(gap_benefit), m_ccal(ccal) {}
        void set_cost_calculator(CostCalculator ccal) { m_ccal = ccal;}
        template <class Sequence0, class Sequence1>
        Alignment global_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment cut_one_end_off_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment cut_one_begin_off_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment initial_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment end_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment inside_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment semi_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment partial_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
    };

/*
   The gap_benefit should be negative. Otherwise, the results are not
   guaranteed to be the optimal solution.

   The align functions differ in what part of the sequence is aligned.
   The freedom of taking a subsequence increases when going down, so the
   score may only go up (or stay the same) when going in that direction.

    global_align: align both complete sequences. start[i]==0 and
            end[i]==size of seq_i, for i==1 and 2.
    cut_one_end_off_align: align the start of the sequences.
            start[0]==0, start[1]==0 and either
            end[0]==size of seq_0 or end[1]==size of seq_1.
    cut_one_begin_off_align: align the end of the sequences.
            end[0]==size of seq_0, end[1]==size of seq_1 and
            either start[0]==0 or start[1]==0.
    initial_align: align the start of the sequences.
            start[0]==0 and start[1]==0.
    end_align: align the end of the sequences.
            end[0]==size of seq_0 and end[1]==size of seq_1.
    inside_align: align seq_0 inside seq_1. start[0]==0 and
            end[0]==size of sequence 0.
    semi_align: start[0]==0 or start[1]==0
            end[0]==size of seq_0 or end[1]==size of seq_1.
    partial_align: No requirement for start and end.

   */

    template <class CostCalculator>
    class AffineAligner {
    public:
        AffineAligner(double gap_start_cost, double gap_extension_cost,
                CostCalculator ccal = CostCalculator())
        : m_gap_start(gap_start_cost), m_gap_ext(gap_extension_cost),
        m_ccal(ccal)
        {}
        void set_cost_calculator(CostCalculator ccal) { m_ccal = ccal;}

        template <class Sequence0, class Sequence1>
        Alignment global_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment cut_one_end_off_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment cut_one_begin_off_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment initial_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment end_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment inside_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment semi_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
        template <class Sequence0, class Sequence1>
        Alignment partial_align(Sequence0 const &seq_0,
                Sequence1 const &seq_1) const;
    };

}


/*
Requirements for template classes

The alignment algorithms are templatized by classes CostCalculator,
Sequence0 and Sequence1.

Usually, the two sequence classes will be the same, or at least they will
have the same value type.

In order to compile, those classes should have certain features.  If you
can call the following function template with your cost calculator and
sequences, you should be OK. More detailed requirements are listed after
the function.
*/

#include "boost/range.hpp"

    template <class CostCalculator, class Sequence0, class Sequence1>
    void usage(CostCalculator const &ccal,
            Sequence0 const &seq_0, Sequence1 const &seq_1)
    {
        // Copy constructor
        CostCalculator ccal_copy(ccal);
        // assignment operator
        ccal_copy = ccal;
        ccal.preprocess(seq_0, seq_1);
        size_t sz1 = boost::distance(seq_0);
        size_t sz2 = boost::distance(seq_1);
        auto s1cur = boost::const_begin(seq_0);
        for (size_t i=0; i!=sz1; ++i) {
            auto s2cur = boost::const_begin(seq_1));
            for (size_t j=0; j!=sz2; ++j) {
                double benefit = ccal.match(*s1cur, *s2cur);
                ++s2cur;
            }
            ++s1cur;
        }
    }

/*
   The classes Sequence0 and Sequence1 should fulfill the requirements for
   a boost Forward Range. See the documentation of the range library on
   http://www.boost.org for full details. A range can be thought of as a
   'container light'. It should have a 'begin' and 'end' which gives a
   forward iterator.

   The CostCalculator class should have a copy constructor and an
   assignment operator. It should have a member function 'match', which
   takes an element of sequence1 and an element of sequence2 and returns a
   double. This value is a measure for how well the two elements match. A
   good match should yield a (big) positive value. A bad match should yield
   a negative value.
   Furthermore the costcalculator class should have a member function
   'preprocess', which takes two sequences. This member function is
   guaranteed to be called before any call to 'match' for those sequences.
   This may be used to do some preprocessing of the sequences. For
   instance, say we want to compare two melodies, where a melody is a
   sequnces of notes and a key. If one of the melodies is in C and the
   other one is in G, we might want to transpose the latter melody. This
   can be done in the preprocess step.
   */
